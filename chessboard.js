const makeChessboard = () => {
  let chessboard = [];
  const listBuahCatur = ["B", "K", "M", "RT", "RJ", "M", "K", "B"];

  // ... write your code here
  for (let i = 0; i < 8; i++) {
    tempRow = [];
    for (let j = 0; j < 8; j++) {
      if (i == 1 || i == 6) {
        i === 1 ? tempRow.push("P Black") : tempRow.push("P White");
      } else if (i === 0 || i === 7) {
        i === 0
          ? tempRow.push(`${listBuahCatur[j]} Black`)
          : tempRow.push(`${listBuahCatur[j]} White`);
      } else {
        tempRow.push("-");
      }
    }
    chessboard.push(tempRow);
  }

  return chessboard;
};

const printBoard = (x) => {
  // ... write your code here
  console.log("Chess Board");
  console.log(x); //print all board
  console.log("Input Specific Location with delimiter ',' Ex: '7,2'");
  const readline = require("readline").createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  readline.question("Your Specific Position : ", (userInput) => {
    arrayUserInput = userInput.split(",");
    if (arrayUserInput.length === 2) {
      if (
        arrayUserInput[0] >= 0 &&
        arrayUserInput[0] < 8 &&
        arrayUserInput[1] >= 0 &&
        arrayUserInput[1] < 8
      ) {
        console.log(
          `x[${arrayUserInput[0]}][${arrayUserInput[1]}] => ${
            x[arrayUserInput[0]][arrayUserInput[1]]
          }`
        );
      } else {
        console.log("Position must in range 0 - 7");
      }
    } else {
      console.log("Error! Please make sure you're following the format");
    }
    readline.close();
  });
};

printBoard(makeChessboard());
